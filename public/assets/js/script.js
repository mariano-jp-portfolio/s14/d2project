// // // Two-dimensional array
// let todo = [
// 	["work", "completed"],
// 	["play games", "on-going"],
// 	["sleep", "full"]
// ];

// // console.log(todo[0][0]);
// // console.log(todo[1][0]);

// todo.push(["study", "on-going"]);
// // todo.pop();

// console.table(todo);


// // Iterating elements

// Outer loop
// for (let i = 0; i < todo.length; i++) {
// 	// Get size of the inner array
// 	innerArrayLength = todo[i].length;
// 	// Inner loop
// 	for (let j = 0; j < innerArrayLength; j++) {
// 		console.log("[" + i + ", " + j + "]" + " " + todo[i][j]);
// 	}
// }

// console.table(todo);


// Mini activity
/*
	1. Create a 2d array that will display the name of at least five students and their test scores. The maximum test score is 50.
	2. Calculate the % of test score using the following formula:
		test % = (test score / maximum test score) * 100
		
	Expected Output:
		Joe Doe		45	80%
		John Doe	40	80%
*/

let gradeArray = [];

const toGradeArray = (name, score) => {
	let grades = (score / 50) * 100;
	gradeArray.push([name, score, grades]);
	console.table(gradeArray);
}

// To test
toGradeArray("Kyle", 49);
toGradeArray("Ramon", 45);
toGradeArray("Taylor", 38);
toGradeArray("Bill", 42);
toGradeArray("Victor", 47);